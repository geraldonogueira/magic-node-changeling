import * as gitaxianProbe from '../services/gitaxian-probe';
import chalk from 'chalk';
import fs from 'fs';

export const getAvailablePauperDecks = async (): Promise<void> => {
    console.log(chalk.bgBlueBright('Starting retrieval...'));
    const archetypes = await gitaxianProbe.getPauperArchetypes();

    if (!fs.existsSync('./_decks-pauper'))
        await fs.mkdirSync('./_decks-pauper');

    let counter = 1;
    for (const archetype of archetypes) {
        console.log(chalk.blueBright(`Retrieving [${archetype.name}]...`));
        let deck = await gitaxianProbe.getDeckList(archetype.url);
        
        const mappedDeck = {
            main: deck.main.map(card => card.name),
            side: deck.side.map(card => card.name)
        }

        const formattedCounter = counter <= 9 ? '0' + counter : counter
        fs.writeFileSync(`./_decks-pauper/${archetype.name}_${formattedCounter}.json`, JSON.stringify(mappedDeck, null, 2));
        console.log(chalk.blueBright(`---------------------------------`));
        counter++;
    }
}