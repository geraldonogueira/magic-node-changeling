export interface Card {
    name: string,
    quantity: number,
}

export interface Deck {
    main: Array<Card>,
    side: Array<Card>
}

export interface Archetype {
    name: string,
    url: string
}