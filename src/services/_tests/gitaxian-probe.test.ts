import "regenerator-runtime/runtime";
import * as gitaxianProbe from '../gitaxian-probe';
import { Archetype } from "../types/deck";
import { mapArchetype } from "../gitaxian-probe";

// because WotC pages sucks to load, geez
const puppeteerTimeout = 120 * 1000;

describe('Scrapping MTGO standings', () => {
    it('builds a valid json => mtgo-standings/pauper-challenge-2019-07-22', async () => {
        const standings = await gitaxianProbe.getPauperChallengeLists();
        const scrappedResult = standings.find(((result: { Name: string; }) => result.Name === 'LuckyOnline'));
        expect(scrappedResult).toStrictEqual({
            Archetype: "Burn",
            Rank: '32',
            Name: 'LuckyOnline',
            Points: '9',
            OMWP: '0.5816',
            GWP: '0.4667',
            OGWP: '0.5254',
            Deck: {
                main: [
                    { name: "Ghitu Lavarunner", quantity: 4, },
                    { name: "Thermo-Alchemist", quantity: 4, },
                    { name: "Chain Lightning", quantity: 4, },
                    { name: "Lava Spike", quantity: 4, },
                    { name: "Rift Bolt", quantity: 4, },
                    { name: "Skewer the Critics", quantity: 4, },
                    { name: "Fireblast", quantity: 4, },
                    { name: "Lightning Bolt", quantity: 4, },
                    { name: "Needle Drop", quantity: 4, },
                    { name: "Searing Blaze", quantity: 4, },
                    { name: "Curse of the Pierced Heart", quantity: 4, },
                    { name: "Forgotten Cave", quantity: 1, },
                    { name: "Mountain", quantity: 15, },
                ],
                side: [
                    { name: "Electrickery", quantity: 1, },
                    { name: "Flaring Pain", quantity: 2, },
                    { name: "Martyr of Ashes", quantity: 2, },
                    { name: "Molten Rain", quantity: 3, },
                    { name: "Pyroblast", quantity: 3, },
                    { name: "Smash to Smithereens", quantity: 4, },
                ]
            }
        });

    }, puppeteerTimeout);

});

describe('Scrapping MTGO archetypes', () => {
    let archetypes: Array<Archetype>;

    beforeAll(async (done) => {
        archetypes = await gitaxianProbe.getPauperArchetypes();
        done();
    });

    it('gets all available pauper archetypes', async () => {
        expect(archetypes.find(archetype => archetype.name === "Tron")).toBeTruthy();
        expect(archetypes.find(archetype => archetype.name === "Elves")).toBeTruthy();
        expect(archetypes.find(archetype => archetype.name === "Burn")).toBeTruthy()

    }, puppeteerTimeout);

    it('gets the latest [Burn] archetype full list', async () => {
        const burn = archetypes.find(archetype => archetype.name === "Burn")
        const burnDeckList = await gitaxianProbe.getDeckList(burn.url);
        expect(burnDeckList.main.find(card => card.name === 'Lightning Bolt').quantity).toBe(4);
        expect(burnDeckList.main.find(card => card.name === 'Chain Lightning').quantity).toBe(4);
    }, puppeteerTimeout);

    it('gets the latest [Tron] archetype full list', async () => {
        const tron = archetypes.find(archetype => archetype.name === "Tron")
        const tronDeckList = await gitaxianProbe.getDeckList(tron.url);
        expect(tronDeckList.main.find(card => card.name === "Urza's Tower").quantity).toBeGreaterThanOrEqual(1);
        expect(tronDeckList.main.find(card => card.name === "Urza's Power Plant").quantity).toBeGreaterThanOrEqual(1);
        expect(tronDeckList.main.find(card => card.name === "Urza's Mine").quantity).toBeGreaterThanOrEqual(1);
    }, puppeteerTimeout);

    it('gets the latest [Elves] archetype full list', async () => {
        const elves = archetypes.find(archetype => archetype.name === "Elves")
        const deckList = await gitaxianProbe.getDeckList(elves.url);
        const archetype = mapArchetype(deckList);
        expect(archetype).toBe("Elves");
    }, puppeteerTimeout);
});