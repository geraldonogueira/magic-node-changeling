import puppeteer from 'puppeteer';
import { Card, Deck, Archetype } from './types/deck';
import * as archetypes from './pauper-archetypes';

interface archetype {
    name: string,
    cards: string[]
}

interface PossibleMatch {
    name: string,
    matches: number,
}

interface MappedStandingResults {
    standingsColumns: string[];
    standingsRows: (string | Deck)[][];
}

interface FinalFormStandingResult {
    Archetype: string,
    Rank: string,
    Name: string,
    Points: string,
    OMWP: string,
    GWP: string,
    OGWP: string,
    Deck: Deck
}

const mapStandingResults = (rawScrappedData: MappedStandingResults): any => {
    return rawScrappedData.standingsRows
        .map(row => {
            // why not laying object? 🤔
            // this needs to be dynamic because it will be built on columns 
            // I don't it will be there
            const standingObject: any = {};

            rawScrappedData.standingsColumns.forEach((column, index) => {
                standingObject[column] = row[index];
            });

            return standingObject;
        })
        .map(mappedResult => {
            return { ...mappedResult, Archetype: mapArchetype(mappedResult.Deck) };
        });
}

const getDeckList = async (deckUrl: string): Promise<Deck> => {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();

    await page.setRequestInterception(true);
    page.on('request', request => {
        if (['image', 'script'].includes(request.resourceType()))
            request.abort();
        else
            request.continue();
    });

    await page.goto(deckUrl);

    const deckList = await page.evaluate(() => {
        const deckTable = document.getElementsByClassName('deck-view-deck-table')[0];
        const allCards = Array.from(deckTable.querySelectorAll('tr'));

        const deck: Deck = {
            main: [],
            side: []
        };

        let sideboardSection = false;

        allCards.forEach(card => {
            // because mtggoldfish needs js to trim their strings and we're blocking those due to insane amount of ads
            // we need to do the trimming ourselves here
            const headerSelector = card.querySelector('td.deck-col-header') as HTMLElement;
            const nameSelector = card.querySelector('td.deck-col-card') as HTMLElement;
            const quantitySelector = card.querySelector('td.deck-col-qty') as HTMLElement;

            const header = headerSelector ? headerSelector.innerText.trim() : '';
            const name = nameSelector ? nameSelector.innerText.trim() : '';
            const quantity = quantitySelector ? quantitySelector.innerText.trim() : '';


            // everything else after the sideboard <tr> is detected will be addded to the sideboard property
            if (header.includes('Sideboard')) sideboardSection = true;
            if (!name) return;
            const property = sideboardSection ? 'side' : 'main';
            deck[property].push({
                quantity: parseInt(quantity),
                name,
            });
        });

        return deck;
    });

    await browser.close();
    return deckList;
}


// TODO: refatorar isso aqui, vei pqp
const mapArchetype = (deck: Deck): string => {
    const cardList = deck.main.map(card => card.name);
    const matchTable: PossibleMatch[] = [];

    Object.keys(archetypes).forEach(archetypeName => {
        const archetype = (archetypes as any)[archetypeName] as archetype; /* TODO: big hack here :( */
        const matches = archetype.cards.filter((value: string) => cardList.includes(value)).length;
        matchTable.push({ name: archetype.name, matches });
    });

    const maxMatches = Math.max(...matchTable.map(match => match.matches));
    const archetype = matchTable.find(match => match.matches === maxMatches).name;

    return archetype || "Unknown";
}


const getPauperArchetypes = async (): Promise<Array<Archetype>> => {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();

    await page.setRequestInterception(true);
    page.on('request', request => {
        if (['image', 'script'].includes(request.resourceType()))
            request.abort();
        else
            request.continue();
    });

    await page.goto('https://www.mtggoldfish.com/metagame/pauper/full#paper');

    const result: Array<Archetype> = await page.evaluate(() => {
        const archetypes = Array.from(
            document.querySelectorAll('div.archetype-tile-description span.deck-price-online a'))
            .map((archetype) => ({ name: archetype.textContent, url: `https://www.mtggoldfish.com/${archetype.getAttribute('href')}` })) as Array<Archetype>;

        return archetypes
    });
    await browser.close();
    return result;
}

const getPauperChallengeLists = async (): Promise<FinalFormStandingResult[]> => {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();

    await page.setRequestInterception(true);
    page.on('request', request => {
        if (request.resourceType() === 'image')
            request.abort();
        else
            request.continue();
    });

    await page.goto('https://magic.wizards.com/en/articles/archive/mtgo-standings/pauper-challenge-2019-07-22');
    // await page.goto('https://magic.wizards.com/en/articles/archive/mtgo-standings/pauper-challenge-2019-08-05');

    await page.waitForSelector('div.rankings-table')

    const result = await page.evaluate(() => {
        const standingsColumns = Array.from(document
            .querySelector('div.rankings-table')
            .querySelectorAll('thead tr th'))
            .map((element) => element.textContent);

        standingsColumns.push('Deck');
        standingsColumns.push('Archetype');

        const standingsRows: (string | Deck)[][] = [];

        document
            .querySelector('div.rankings-table')
            .querySelectorAll('table.sticky-enabled.sticky-enabled.responsive-table.large-only tbody tr')
            .forEach((element) => {
                const mappedStandingsColumnsValues: Array<string | Deck> = Array.from(element.querySelectorAll('td')).map(standingTd => standingTd.innerText);
                const mapDeck = (card: Element): Card => ({
                    name: card.querySelector('.card-name').textContent,
                    quantity: parseInt(card.querySelector('.card-count').textContent)
                });

                const playerName = (mappedStandingsColumnsValues[1] as string).toLowerCase();
                const deckContainerSelector = `div[id^="${playerName}"]`;

                const main = Array.from(document.querySelectorAll(`${deckContainerSelector} div.sorted-by-overview-container span.row`)).map(mapDeck);
                const side = Array.from(document.querySelectorAll(`${deckContainerSelector} div.sorted-by-sideboard-container span.row`)).map(mapDeck);
                const deck = { main, side };

                mappedStandingsColumnsValues.push(deck);
                standingsRows.push(mappedStandingsColumnsValues);
            });

        return {
            standingsColumns,
            standingsRows,
        }
    });

    const mappedResult = mapStandingResults(result);

    await browser.close();
    return mappedResult;
};

export {
    getPauperChallengeLists,
    getPauperArchetypes,
    getDeckList,
    mapArchetype
}