export const jeskai = {
    name: "Astro Jeskai",
    cards: [
        "Arcum's Astrolabe",
        "Dispel",
        "Ephemerate",
        "Lightning Bolt",
        "Ponder",
        "Skred",
        "Counterspell",
    ]
}

export const burn = {
    name: "Burn",
    cards: [
        "Ghitu Lavarunner",
        "Thermo-Alchemist",
        "Chain Lightning",
        "Lava Spike",
        "Lightning Bolt",
        "Needle Drop",
        "Searing Blaze",
        "Rift Bolt",
        "Skewer the Critics",
        "Fireblast",
        "Curse of the Pierced Heart",
        "Mountain"
    ],
}

export const monowhiteheroic = {
    name: "Mono W Heroic",
    cards: [
        "Deftblade Elite",
        "Lagonna-Band Trailblazer",
        "Akroan Skyguard",
        "Seeker of the Way",
        "Defiant Strike",
        "Emerge Unscathed",
        "Mutagenic Growth",
        "Act of Heroism",
        "Spare from Evil",
        "Cartouche of Solidarity",
        "Ethereal Armor",
        "Hyena Umbra",
        "Cho-Manno's Blessing",
        "Plains",
        "Secluded Steppe"
    ]
}

export const ubcontrol = {
    main: "Dimir Control",
    cards: [
        "Augur of Bolas",
        "Mulldrifter",
        "Gurmag Angler",
        "Disfigure",
        "Preordain",
        "Thought Scour",
        "Agony Warp",
        "Chainer's Edict",
        "Counterspell",
        "Diabolic Edict",
        "Doom Blade",
        "Echoing Decay",
        "Mana Leak",
        "Negate",
        "Prohibit",
        "Exclude",
        "Forbidden Alchemy",
        "Probe",
        "Soul Manipulation",
        "Dead Weight",
        "Dimir Aqueduct",
        "Dimir Guildgate",
        "Dismal Backwater",
        "Island",
        "Swamp"
    ]
}

export const affinity = {
    name: "Affinity",
    cards: [
        "Atog",
        "Carapace Forger",
        "Frogmite",
        "Gearseeker Serpent",
        "Myr Enforcer",
        "Galvanic Blast",
        "Fling",
        "Perilous Research",
        "Temur Battle Rage",
        "Metallic Rebuke",
        "Thoughtcast",
        "Chromatic Star",
        "Flayer Husk",
        "Springleaf Drum",
        "Prophetic Prism",
        "Darksteel Citadel",
        "Great Furnace",
        "Seat of the Synod",
        "Tree of Tales",
        "Vault of Whispers"
    ]
}

export const elves = {
    name: "Elves",
    cards: [
        "Birchlore Rangers",
        "Elvish Mystic",
        "Fyndhorn Elves",
        "Llanowar Elves",
        "Nettle Sentinel",
        "Quirion Ranger",
        "Elvish Vanguard",
        "Priest of Titania",
        "Wellwisher",
        "Timberwatch Elf",
        "Lys Alana Huntmaster",
        "Winding Way",
        "Lead the Stampede",
        "Viridian Longbow",
        "Spidersilk Armor",
        "Forest"
    ],
}

export const bogles = {
    name: "Bogles",
    cards: [
        "Gladecover Scout",
        "Slippery Bogle",
        "Silhana Ledgewalker",
        "Aura Gnarlid",
        "Heliod's Pilgrim",
        "Fling",
        "Abundant Growth",
        "Cartouche of Solidarity",
        "Ethereal Armor",
        "Rancor",
        "Spider Umbra",
        "Utopia Sprawl",
        "Fists of Ironwood",
        "Ancestral Mask",
        "Armadillo Cloak",
        "Cartouche of Strength",
        "Ash Barrens",
        "Blossoming Sands",
        "Forest",
        "Khalni Garden",
        "Snow-Covered Plains"
    ]
};

export const mbc = {
    name: "MBC",
    cards: [
        "Cuombajj Witches",
        "Chittering Rats",
        "Whisper Agent",
        "Thorn of the Black Rose",
        "Gray Merchant of Asphodel",
        "Gurmag Angler",
        "Defile",
        "Duress",
        "Geth's Verdict",
        "Sign in Blood",
        "Victim of Night",
        "Tendrils of Corruption",
        "Pestilence",
        "Barren Moor",
        "Swamp"
    ]
}

export const goblins = {
    name: "Goblins",
    cards: [
        "Foundry Street Denizen",
        "Goblin Bushwhacker",
        "Goblin Cohort",
        "Jackal Familiar",
        "Mogg Conscripts",
        "Burning-Tree Emissary",
        "Mudbrawler Cohort",
        "Valley Dasher",
        "Goblin Heelcutter",
        "Chain Lightning",
        "Goblin Grenade",
        "Lightning Bolt",
        "Mountain"
    ]
}

export const bwpestilence = {
    name: "BW Pestilence",
    cards: [
        "Guardian of the Guildpact",
        "Palace Sentinels",
        "Thorn of the Black Rose",
        "Gurmag Angler",
        "Disfigure",
        "Duress",
        "Castigate",
        "Chainer's Edict",
        "Echoing Decay",
        "Night's Whisper",
        "Read the Bones",
        "Unmake",
        "Evincar's Justice",
        "Pristine Talisman",
        "Dead Weight",
        "Journey to Nowhere",
        "Pestilence",
        "Bojuka Bog",
        "Kabira Crossroads",
        "Orzhov Basilica",
        "Plains",
        "Radiant Fountain",
        "Scoured Barrens",
        "Swamp"
    ]
}

export const tron = {
    name: "Tron",
    cards: [
        "Urza's Mine",
        "Urza's Power Plant",
        "Urza's Tower"
    ]
}