import express from 'express';
import { getPauperChallengeLists } from "./services/gitaxian-probe";

const app = express()
const port = 3000

app.get('/', (req, res) => res.send(`
    Hello World!
    Try /decklists
`))

app.get('/decklists', async (req, res) => {
    const challengeLists = await getPauperChallengeLists();
    res.send({
        result: challengeLists
    });
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
// all lists - https://magic.wizards.com/en/content/deck-lists-magic-online-products-game-info

