### Couple things I need to do: 

- [ ] Set a ton of tests for this
- [ ] Typescript?
- [x] Parse standings table into json
- [x] Parse decklists into standings property
- [x] Scrap archetypes into json
- [x] Scrap decklists into json
- [ ] Save deck archetypes in disk
- [ ] Figure out a way of determining deck archetypes

